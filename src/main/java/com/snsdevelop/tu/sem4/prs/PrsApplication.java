package com.snsdevelop.tu.sem4.prs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class PrsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrsApplication.class, args);
	}

}

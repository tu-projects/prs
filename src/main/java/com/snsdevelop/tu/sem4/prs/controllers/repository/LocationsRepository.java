package com.snsdevelop.tu.sem4.prs.controllers.repository;

import com.snsdevelop.tu.sem4.prs.rest.MapQueryLocationService;
import io.spring.guides.gs_producing_web_service.Locations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LocationsRepository {

    private final MapQueryLocationService mapQueryLocationService;

    @Autowired
    public LocationsRepository(MapQueryLocationService mapQueryLocationService) {
        this.mapQueryLocationService = mapQueryLocationService;
    }

    public Locations findLocationsByQuery(String name) {
        return mapQueryLocationService.getLocationsInfoByQuery(name);
    }
}
package com.snsdevelop.tu.sem4.prs.controllers.repository;

import com.snsdevelop.tu.sem4.prs.rest.MapQueryLocalInsightsService;
import io.spring.guides.gs_producing_web_service.InsightLocations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LocalInsightsRepository {

    private final MapQueryLocalInsightsService mapQueryLocalInsightsService;

    @Autowired
    public LocalInsightsRepository(MapQueryLocalInsightsService mapQueryLocalInsightsService) {
        this.mapQueryLocalInsightsService = mapQueryLocalInsightsService;
    }

    public InsightLocations findLocalInsightsByWaypointAndMaxTime(String waypoint, int maxTime) {
        return mapQueryLocalInsightsService.getLocationsInfoByQuery(waypoint, maxTime);
    }
}
package com.snsdevelop.tu.sem4.prs.rest;

import io.spring.guides.gs_producing_web_service.Insight;
import io.spring.guides.gs_producing_web_service.InsightLocations;
import io.spring.guides.gs_producing_web_service.InsightType;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class MapQueryLocalInsightsService {

    private final RestTemplate restTemplate;

    @Value("${bing_key}")
    String key;

    @Autowired
    public MapQueryLocalInsightsService() {
        this.restTemplate = new RestTemplate();
    }

    public InsightLocations getLocationsInfoByQuery(String waypoint, int maxTime) {

        String resp = restTemplate.getForObject("http://dev.virtualearth.net/REST/V1/Routes/LocalInsights?" +
                "Waypoint=" + waypoint + "&" +
                "TravelMode=Driving&" +
                "Optimize=time&MaxTime=" + maxTime + "&" +
                "TimeUnit=Minute&" +
                "type=EatDrink,SeeDo,Shop,BanksAndCreditUnions,Hospitals,HotelsAndMotels,Parking&" +
                "key="+key, String.class);
        JSONObject objectList = new JSONObject(resp);

        InsightLocations locations =  new InsightLocations();

        JSONArray insightTypes =
                objectList.getJSONArray("resourceSets")
                .getJSONObject(0)
                .getJSONArray("resources")
                .getJSONObject(0)
                .getJSONArray("categoryTypeResults");

        for (int i = 0; i < insightTypes.length(); i++) {
            JSONObject insightTypeObject = insightTypes.getJSONObject(i);

            InsightType insightType = new InsightType();
            insightType.setType(insightTypeObject.getString("categoryTypeName"));
            insightType.setDescription(insightTypeObject.getString("categoryTypeSummary"));

            JSONArray insightsObject = insightTypeObject.getJSONArray("entities");

            for (int k = 0; k < insightsObject.length(); k++) {
                try {


                    JSONObject insightObject = insightsObject.getJSONObject(k);

                    Insight insight = new Insight();
                    insight.setName(insightObject.getString("entityName"));
                    insight.setLat(insightObject.getDouble("latitude"));
                    insight.setLon(insightObject.getDouble("longitude"));

                    insightType.getInsights().add(insight);
                }catch (JSONException e) {
                    System.out.println(e);
                }
            }

            locations.getInsightTypes().add(insightType);
        }

        locations.getInsightTypes();
        return locations;

    }
}

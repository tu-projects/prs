package com.snsdevelop.tu.sem4.prs.rest;

import io.spring.guides.gs_producing_web_service.Location;
import io.spring.guides.gs_producing_web_service.Locations;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class MapQueryLocationService {

    private final RestTemplate restTemplate;

    @Value("${bing_key}")
    String key;

    @Autowired
    public MapQueryLocationService() {
        this.restTemplate = new RestTemplate();
    }

    public Locations getLocationsInfoByQuery(String s) {
        String resp = restTemplate.getForObject("http://dev.virtualearth.net/REST/v1/Locations?q="  + s +"&o=json&key="+key, String.class);
        JSONObject objectList = new JSONObject(resp);

        Locations locations = new Locations();

        JSONObject totalSet =
                objectList.getJSONArray("resourceSets")
                        .getJSONObject(0);

        int resourcesSize = totalSet.getInt("estimatedTotal");

        JSONArray resources =
                totalSet
                .getJSONArray("resources");

        for (int i = 0; i < resourcesSize; i++) {

            JSONObject o = resources.getJSONObject(i);

            Location location = new Location();
            location.setName(o.getString("name"));
            location.setLat(o.getJSONObject("point").getJSONArray("coordinates").getDouble(0));
            location.setLon(o.getJSONObject("point").getJSONArray("coordinates").getDouble(1));
            location.setAddress(o.getJSONObject("address").getString("adminDistrict") + ", "
                    + o.getJSONObject("address").getString("countryRegion"));
            location.setConfidence(o.getString("confidence"));
            location.setEntityType(o.getString("entityType"));

            locations.getLocationDetails().add(location);
        }

        return locations;

    }
}

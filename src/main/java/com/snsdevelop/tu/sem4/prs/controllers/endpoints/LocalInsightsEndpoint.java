package com.snsdevelop.tu.sem4.prs.controllers.endpoints;

import com.snsdevelop.tu.sem4.prs.controllers.repository.LocalInsightsRepository;
import io.spring.guides.gs_producing_web_service.GetLocalInsightsRequest;
import io.spring.guides.gs_producing_web_service.GetLocalInsightsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;


@Endpoint
public class LocalInsightsEndpoint extends com.snsdevelop.tu.sem4.prs.controllers.endpoints.Endpoint {

    private LocalInsightsRepository localInsightsRepository;

    @Autowired
    public LocalInsightsEndpoint(LocalInsightsRepository localInsightsRepository) {
        this.localInsightsRepository = localInsightsRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getLocalInsightsRequest")
    @ResponsePayload
    public GetLocalInsightsResponse getLocationsByQuery(@RequestPayload GetLocalInsightsRequest request) {
        GetLocalInsightsResponse response = new GetLocalInsightsResponse();
        response.setInsightLocations(localInsightsRepository.findLocalInsightsByWaypointAndMaxTime(request.getWaypoint(), request.getMaxTime()));

        return response;
    }
}
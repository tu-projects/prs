package com.snsdevelop.tu.sem4.prs.controllers.endpoints;

import com.snsdevelop.tu.sem4.prs.controllers.repository.LocationsRepository;
import io.spring.guides.gs_producing_web_service.GetLocationsByQueryRequest;
import io.spring.guides.gs_producing_web_service.GetLocationsByQueryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;


@Endpoint
public class LocationsEndpoint extends com.snsdevelop.tu.sem4.prs.controllers.endpoints.Endpoint {

    private LocationsRepository locationsRepository;

    @Autowired
    public LocationsEndpoint(LocationsRepository locationsRepository) {
        this.locationsRepository = locationsRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getLocationsByQueryRequest")
    @ResponsePayload
    public GetLocationsByQueryResponse getLocationsByQuery(@RequestPayload GetLocationsByQueryRequest request) {
        GetLocationsByQueryResponse response = new GetLocationsByQueryResponse();
        response.setLocations(locationsRepository.findLocationsByQuery(request.getQuery()));

        return response;
    }
}